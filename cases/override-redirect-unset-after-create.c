#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <xcb/xcb.h>

int main(void) {
	xcb_connection_t *conn = xcb_connect(NULL, NULL);
	xcb_screen_t *screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;

	static const uint16_t win_w = 120;
	static const uint16_t win_h = 120;

	xcb_window_t win = xcb_generate_id(conn);

	const uint16_t win_x = screen->width_in_pixels / 2 - win_w / 2;
	const uint16_t win_y = screen->height_in_pixels / 2 - win_h / 2;

	const uint32_t color = 0xFF0000FF;

	const uint32_t values[] = {
		color,
		1,
	};
	xcb_create_window(conn, XCB_COPY_FROM_PARENT, win, screen->root, win_x, win_y, win_w, win_h, 0,
			XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual,
			XCB_CW_BACK_PIXEL | XCB_CW_OVERRIDE_REDIRECT, values);

	xcb_flush(conn);
	sleep(1);

	xcb_change_window_attributes(conn, win, XCB_CW_OVERRIDE_REDIRECT, &(uint32_t[]){0});

	xcb_flush(conn);
	sleep(1);

	xcb_map_window(conn, win);

	xcb_flush(conn);
	sleep(1);

	return 0;
}
