#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <xcb/xcb.h>

int main(void) {
	xcb_connection_t *conn = xcb_connect(NULL, NULL);
	xcb_screen_t *screen = xcb_setup_roots_iterator(xcb_get_setup(conn)).data;

	static const uint16_t win_w = 120;
	static const uint16_t win_h = 120;

	xcb_window_t wins[64];
	static const size_t n_wins = sizeof(wins) / sizeof(*wins);

	const uint16_t win_x_step = 10;
	const uint16_t win_y_step = 8;
	const uint16_t win_x_base = (screen->width_in_pixels - ((n_wins - 1) * win_x_step + win_w)) / 2;
	const uint16_t win_y_base =
			(screen->height_in_pixels - ((n_wins - 1) * win_y_step + win_h)) / 2;

	for (size_t i = 0; i < n_wins; i++) {
		xcb_window_t win = xcb_generate_id(conn);

		uint32_t color = 0xFF000000 | // alpha
				(!!(i & 0x1) * 0x00FF0000) | // red
				(!!(i & 0x2) * 0x000FF00) | // green
				(!!(i & 0x4) * 0x00000FF); // blue

		const uint16_t win_x = win_x_base + win_x_step * i;
		const uint16_t win_y = win_y_base + win_y_step * i;
		const uint32_t values[] = {
			color,
			1,
		};
		xcb_create_window(conn, XCB_COPY_FROM_PARENT, win, screen->root, win_x, win_y, win_w, win_h,
				0, XCB_WINDOW_CLASS_INPUT_OUTPUT, screen->root_visual,
				XCB_CW_BACK_PIXEL | XCB_CW_OVERRIDE_REDIRECT, values);

		wins[i] = win;
	}

	for (size_t i = 0; i < n_wins; i++) {
		xcb_map_window(conn, wins[i]);
	}

	xcb_flush(conn);
	sleep(3);

	return 0;
}
